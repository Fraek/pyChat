import socket
import struct
import threading

class Client(threading.Thread):
    SEND_BUFFER = 4096

    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.running = True

    def _connect_socket(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.connect((self.host, self.port))

        threading.Thread(target=self.listenToServer, args=[self.server_socket]).start()

    def _run(self):
        while self.running:
            user_input = raw_input('I> ')
            self._send(user_input)

    def listenToServer(self, socket):
        while self.running:
            try:
                data = socket.recv(self.SEND_BUFFER)
                if data:
                    response = data
                    print (response)
                else:
                    print ("no data")
            except:
                socket.close()
                return False

    def run(self):
        self._connect_socket()
        self._run()

    def _send(self, message):
        msg = struct.pack('>I', len(message)) + message
        self.server_socket.sendall(msg)


def main():
    host = raw_input('Host:')
    port = int(raw_input('Port:'))
    client = Client(host, port)
    client.start()


if __name__ == '__main__':
    main()
